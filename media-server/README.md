# Media server

Current state is not yet perfect, there's some file permission issues when sharing the same volume,
the (not ideal) solution is to give all permissions to some directories ( chmod 777 )

There's an open issue for this



## Table of services

| Service   	| Port  	| Example url            	| Documentation                      	|
|-----------	|-------	|------------------------	|------------------------------------	|
| Plex      	| 32400 	| http://localhost:32400 	| https://www.plex.tv/               	|
| Sonarr    	| 8989  	| http://localhost:8989  	| https://sonarr.tv/                 	|
| Radarr    	| 7878  	| http://localhost:7878  	| https://radarr.video/              	|
| Jackett   	| 9117  	| http://localhost:9117  	| https://github.com/Jackett/Jackett 	|
| Qbit      	| 8080  	| http://localhost:8080  	| https://www.qbittorrent.org/       	|
| Portainer 	| 9000  	| http://localhost:9000  	| https://www.portainer.io/          	|
| Traefik   	| 7080  	| http://localhost:7080  	| https://containo.us/traefik/         	|
| Traefik   	| 80    	| http://localhost      	| https://containo.us/traefik/         	|


# Getting started

## pi-hole integration

If you are running a pi-hole it's easy to add these services to you home network

```bash
$ sudo vim /etc/pihole/local.list
```

For the next example we are running the docker-compose on the server with the IP: 192.168.0.220
```code
192.168.0.176 raspberrypi
192.168.0.176 pi.hole
192.168.0.220 jackett-media
192.168.0.220 sonarr-media
192.168.0.220 qbit-media 
192.168.0.220 plex-media
192.168.0.220 portainer-media
192.168.0.220 traefik-media 
```

After changin the file restart the pi-hole dns
```bash
$ pihole restartdns
```


## Requirements

Make sure you have docker installed and updated, if you don't have docker instaled follow [this guide](https://docs.docker.com/install/)

Once docker is installed, open you terminal and run the following command on this directory:

```bash
$ docker-compose up -d
```

## Configuration

Start by configuring jackett and qbit as it's needed by Sonnar and Radarr

### Qbit

WIP

### Jacket

WIP

### Sonarr

WIP

### Radarr

WIP

### Plex

WIP

### Portainer


