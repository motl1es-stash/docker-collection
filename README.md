# docker-collection

## Media server

Using docker, setups an automated plex media server, uses sonarr and radarr to find, download and setup the media files

For more documentation check [media-server README](media-server/README.md)
